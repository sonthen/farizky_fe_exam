import { SimpleCommercePage } from './app.po';

describe('simple-commerce App', () => {
  let page: SimpleCommercePage;

  beforeEach(() => {
    page = new SimpleCommercePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
