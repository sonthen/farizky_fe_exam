import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router'; //*//
import { AppComponent } from './app.component';




import { DataService } from './services/data.service';
import { ApiService } from './services/api.service';
import { UserComponent } from './user/user.component';



@NgModule({
  declarations: [
    AppComponent,
   
    UserComponent,

  ],
  imports: [
    BrowserModule,HttpModule 
  ],
    providers: [DataService,ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
