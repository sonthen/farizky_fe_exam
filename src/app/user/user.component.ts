import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  data:object[];
  constructor( private api:ApiService) { }

  ngOnInit() {
     this.api.getData()
   .subscribe( result => this.data = result.json());
  }

  delete(index){
    this.user.splice(index, 1);
  }
}
